#! /bin/bash

THEMES_DIR=color_themes
CSS_LINK_NAME=theme_colors.css
IMPORTS_DIR=imports
IMPORT_FILE=import.css
IMPORT_FILE_HONOR_PLASMA=import_honor_plasma.css
IMPORT_FILE_IGNORE_PLASMA=import_ignore_plasma.css

PLASMA_ACTIVE=plasma_active
PLASMA_INACTIVE=plasma_inactive

PLASMA_SOURCE_CSS_FILE=~/.config/gtk-3.0/colors.css
LINK_TO_PLASMA_SOURCE_FILE=plasma_colors.css

maxParams=1

usage() {

        echo -e "Usage: $(basename "$0") [--current-status] \n" >&2
        echo -e "Toggles honoring Plasma's color theme on or off. If there is no color theme for GTK generated by Plasma, it won't be toggled on" >&2
        echo -e "\nOption --current-status tells if the theme is currently honoring Plasma color scheme or not" >&2

}


if [ $# -gt $maxParams ]
then
    echo -e "\nError: Incorrect usage." >&2
    usage
    exit 1
fi

if [ $# -eq 1 ]
then
         if [ $1 = "--current-status" ]
         then
         	[ -e $IMPORTS_DIR/PLASMA_ACTIVE ] && echo -e "The theme is honoring Plasma's color scheme\n" || echo -e "\nThe theme is NOT honoring Plasma's color scheme\n"
         	exit 0
         else
             echo -e "\nError: Incorrect usage." >&2
	    usage
	    exit 1
	 fi
fi


# Making sure we are in the script's directory, otherwise it's not going to work
SCRIPT_PATH=`realpath "$0"`
SCRIPT_DIR=`dirname "$SCRIPT_PATH"`
cd $SCRIPT_DIR



cd $IMPORTS_DIR

#if we are honoring Plasma colors...
if [ -e $PLASMA_ACTIVE ]
then
	# deactivate it
	[ -h $IMPORT_FILE ] && rm $IMPORT_FILE
	ln -s $IMPORT_FILE_IGNORE_PLASMA $IMPORT_FILE
	rm $PLASMA_ACTIVE
	touch $PLASMA_INACTIVE
	echo -e "\n Plasma theme honoring toggled OFF (that is: GTK theme will not follow current Plasma color scheme)\n"
else
	#before activating Plasma, check if plasma colors.css file exists!
	if [ ! -e $PLASMA_SOURCE_CSS_FILE ]
	then
		echo -e "Could not find Plasma color theme for GTK ($PLASMA_SOURCE_CSS_FILE), so GTK theme will continue not attempting to follow Plasma color scheme. Not doing anything, therefore..."
		cd cd $SCRIPT_DIR
		exit 2
	fi
	[ -h $IMPORT_FILE ] && rm $IMPORT_FILE
	ln -s $IMPORT_FILE_HONOR_PLASMA $IMPORT_FILE
		
	[ -h $LINK_TO_PLASMA_SOURCE_FILE ] && rm $LINK_TO_PLASMA_SOURCE_FILE
	ln -s $PLASMA_SOURCE_CSS_FILE $LINK_TO_PLASMA_SOURCE_FILE
	
	[ -e $PLASMA_INACTIVE ] && rm $PLASMA_INACTIVE
	touch $PLASMA_ACTIVE
	echo -e "\n Plasma theme honoring toggled ON (that is: GTK theme will follow current Plasma color scheme)\n"
fi 
cd $SCRIPT_DIR