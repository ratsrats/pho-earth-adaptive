# pho-earth-adaptive

![small preview](previews/mosaic-logo.png)

This is a skeuomorphic GTK3 theme that follows your KDE Plasma color scheme, the same way that Breeze GTK theme adapts to it. So if you want to change its colors, you only need to change KDE Plasma's color scheme, nothing else.

Do you want to keep using your venerable Oxygen or QtCurve widgets with Plasma, with any color scheme but can't find an appropriate GTK3 theme that matches it? Seek no more!

This theme is a modification of my [Pho-Earth-by-night](https://www.pling.com/p/1459383/), changing color names to match breeze's and some other improvements. At the same time, it is based on the wonderful pho series by GUILMOUR, such as Pho-Myrtus [here](https://www.pling.com/s/Gnome/p/1240355/). It has currently departed very far away from that theme.

If you don't have Plasma KDE installed, colors will default to Pho-Earth.

Sadly, GTK2 can't follow plasma KDE color scheme because the trick used by Plasma KDE for GTK3 is with the file ~/.config/colors.css, but this only affects GTK3 theme, not GTK2 or GTK4. Currently GTK2 uses Pho-Earth-by-night's GTK2 fixed colors.

GTK4 version is still under construction. It will probably look good enough most times, but there are things to be fixed (still a lot of warnings and errors, for example). Please comment if there's anything to be fixed other than said errors and warnings. I'm still learning about it by myself, by trial and error (is there any documentation that explains most if not all the changes from GTK3? Please tell me in the comments, too).

Unfortunately, it seems the trick to make GTK3 use KDE Plasma's colors does not work for GTK4. The only thing I've done so far is the following: I've addedd a theme\_colors.css link at the root of the theme and both GTK3 and GTK4 themes read this file to use its colors. This link points to a CSS with a color theme in the color\_themes folder. It's a simple file, very easy and clear to edit (you just need how to define colors in RGB hexadecimal values, but any online color picker will help you with that). GTK3 will still adapt to KDE Plasma color scheme. Currently there are two themes, and you can chose which theme to you want to be active by running the script change\_theme.sh. You can add more themes, and the script will help you with chosing (or you can manually remove old link and create the new one, too)


Each preview was obtained just by changing current Plasma color scheme.

Enjoy!

(some keywords to help finding this theme: non-flat, 3D, 3-D, adaptive, adapts, multiple colors, multicolor, shadows, highlights)

## Big previews:

Current style:

![big preview with current style](previews/GTK3WF-05-NewStyle-pho-earth.png)
![big preview with current style, dark version](previews/GTK3WF-05-NewStyle-Earth-by-night.png)
![big preview with current style, oxygen color scheme](previews/GTK3WF-05-NewStyle-Oxygen.png)

Old previews, but with different Plasma color schemes:

![old preview](previews/GTK3WF-1-KvDarkRed.png)
![old preview](previews/GTK3WF-2-OpenSuse-darkened.png)
![old preview](previews/GTK3WF-3-Earthen2.png)
![old preview](previews/mosaic-GTK3WF-3-KvCyan.png)
![old preview](previews/mosaic-GTK3WF-2-Grass.png)
